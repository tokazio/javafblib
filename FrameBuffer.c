#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include "FrameBuffer.h"

// ---------------------------------------------------------------------------------------------------------------------
// Handle structure from Java:
// ---------------------------------------------------------------------------------------------------------------------
struct deviceInfo {
	char *deviceName;				
	int fbfd;						
	int width;
	int height;
	int bpp;						
    long int pagesize;
	char *fbp;						
	struct fb_var_screeninfo vinfo;
};

// To be passed in vsync fb function (need to be a pointer to work)
static __u32 dummy = 0;

//Number of page buffer
static int nbBuff = 2;

//Current page buffer
static int cur_page = 0;

// ---------------------------------------------------------------------------------------------------------------------
//	long	openDevice(String device);
// ---------------------------------------------------------------------------------------------------------------------
JNIEXPORT jlong JNICALL FrameBuffer_openDevice(
		JNIEnv *env, jobject obj, jstring device) {
	jboolean isCopy;
	struct deviceInfo *di;
	di = malloc(sizeof(*di));
	memset(di, 0, sizeof(*di));
	const char *s = (*env)->GetStringUTFChars(env, device, &isCopy);
	di->deviceName = strdup(s);
	if (isCopy)
		(*env)->ReleaseStringUTFChars(env, device, s);
	struct fb_fix_screeninfo finfo;
	di->fbfd = open(di->deviceName, O_RDWR);
	if (!di->fbfd) {
		printf("Error: cannot open framebuffer device. %s\n", di->deviceName);
		return (1);
	}
	// Get fixed screen information
	if (ioctl(di->fbfd, FBIOGET_FSCREENINFO, &finfo)) {
		printf("Error reading fixed information.\n");
		return (2);
	}
	// Get variable screen information
	if (ioctl(di->fbfd, FBIOGET_VSCREENINFO, &di->vinfo)) {
		printf("Error reading variable information.\n");
		return (3);
	}

	di->vinfo.xres_virtual = di->vinfo.xres;
		di->vinfo.yres_virtual = di->vinfo.yres * nbBuff; 
		if(ioctl(di->fbfd, FBIOPUT_VSCREENINFO, &di->vinfo)){
		printf("Error setting double buffer size.\n");
		return(4);
	}

	di->width = di->vinfo.xres;
	di->height = di->vinfo.yres;
	di->bpp = di->vinfo.bits_per_pixel;
    di->pagesize = di->vinfo.xres * di->vinfo.yres * sizeof(int);
	// map framebuffer to user memory
	di->fbp = (char*) mmap(0, finfo.smem_len, PROT_READ | PROT_WRITE, MAP_SHARED, di->fbfd, 0);
	if ((int) di->fbp == -1) {
		printf("Failed to mmap.\n");
		return (5);
	}
    memset(di->fbp,0, di->pagesize);
	return (jlong) (intptr_t) di;
}

// ---------------------------------------------------------------------------------------------------------------------
//	void		closeDevice(long di);
// ---------------------------------------------------------------------------------------------------------------------
JNIEXPORT void JNICALL FrameBuffer_closeDevice(
		JNIEnv *env, jobject obj, jlong jdi) {
	struct deviceInfo *di = (struct deviceInfo *) (intptr_t) jdi;
	free(di->deviceName);
	if (di->fbfd != 0) {
		munmap(di->fbp, di->pagesize);
		close(di->fbfd);
	}
	memset(di, 0, sizeof(*di));
}

// ---------------------------------------------------------------------------------------------------------------------
//	int		getDeviceWidth(long di);
// ---------------------------------------------------------------------------------------------------------------------
JNIEXPORT jint JNICALL FrameBuffer_getDeviceWidth(
		JNIEnv *env, jobject obj, jlong jdi) {
	struct deviceInfo *di = (struct deviceInfo *) (intptr_t) jdi;
	return di->width;
}

// ---------------------------------------------------------------------------------------------------------------------
//	int		getDeviceHeight(long di);
// ---------------------------------------------------------------------------------------------------------------------
JNIEXPORT jint JNICALL FrameBuffer_getDeviceHeight(
		JNIEnv *env, jobject obj, jlong jdi) {
	struct deviceInfo *di = (struct deviceInfo *) (intptr_t) jdi;
	return di->height;
}

// ---------------------------------------------------------------------------------------------------------------------
//	int		getDeviceBitsPerPixel(long di);
// ---------------------------------------------------------------------------------------------------------------------
JNIEXPORT jint JNICALL FrameBuffer_getDeviceBitsPerPixel(
		JNIEnv *env, jobject obj, jlong jdi) {
	struct deviceInfo *di = (struct deviceInfo *) (intptr_t) jdi;
	return di->bpp;
}

// ---------------------------------------------------------------------------------------------------------------------
//	boolean	updateDeviceBuffer(long di,int[] buffer);
// ---------------------------------------------------------------------------------------------------------------------
JNIEXPORT jboolean JNICALL FrameBuffer_updateDeviceBuffer(
		JNIEnv *env, jobject obj, jlong jdi, jintArray buf) {
	struct deviceInfo *di = (struct deviceInfo *) (intptr_t) jdi;
	jint *body = (*env)->GetPrimitiveArrayCritical(env, buf, 0);
	switch (di->bpp) {
        case 32: {
            memcpy(di->fbp+(di->pagesize*cur_page), body, di->pagesize);
			//pixel page position (page height * page index)
			di->vinfo.yoffset = cur_page * di->vinfo.yres;
			//pan screen
			if (ioctl(di->fbfd, FBIOPAN_DISPLAY, &di->vinfo)){
				printf("pan display failed\n");
			}
			//Wait vsync
			if(ioctl(di->fbfd, FBIO_WAITFORVSYNC, &dummy)){
				printf("wait vsync failed\n");
			}
			//next page
			cur_page = (cur_page + 1) % nbBuff;
		}
        break;
		default:
			fprintf(stderr, "FrameBuffer depth %d not supported, use 32bpp\n", di->bpp);
	}
	(*env)->ReleasePrimitiveArrayCritical(env, buf, body, 0);
	return 1;
}

/*
 * Write a pixel to a framebuffer
 */
JNIEXPORT void JNICALL FrameBuffer_writeRGB
(JNIEnv *env, jclass clazz, jlong ptr, jint idx, jint rgb) {
	struct FrameBufferData	*di = (struct FrameBufferData *) (intptr_t) ptr;
	unsigned char *p = (unsigned char *) di->fbp;
	unsigned char *q = (unsigned char *) di->cache;
	unsigned int width;

	if(di->bpp == 8)
		width = 1;
	else if(di->bpp == 16)
		width = 2;
	else if(di->bpp == 24)
		width = 3;
	else if(di->bpp == 32)
		width = 4;
	else
		return;

	p += (width * idx);
	q += (width * idx);
	while(width > 0) {
		if(*q != (unsigned char)(0xFF & rgb))
			*q = *p = (unsigned char)(0xFF & rgb);
		rgb = rgb >> 8;
		width--;
		p++;
		q++;
	}
}

/*
 * Read a pixel from a framebuffer
 */
JNIEXPORT jint JNICALL FrameBuffer_readRGB
(JNIEnv *env, jclass clazz, jlong ptr, jint idx) {
	struct FrameBufferData	*di = (struct FrameBufferData *) (intptr_t) ptr;
	unsigned char *q = (unsigned char *) di->cache;
	unsigned int width;

	if(di->bpp == 8)
		width = 1;
	else if(di->bpp == 16)
		width = 2;
	else if(di->bpp == 24)
		width = 3;
	else if(di->bpp == 32)
		width = 4;
	else
		return -1;

	unsigned int rgb = 0;

	q += (width * idx);
	q += width - 1;
	while(width > 0) {
		rgb = (rgb << 8) + *q;
		width--;
		q--;
	}

	return rgb;
}
