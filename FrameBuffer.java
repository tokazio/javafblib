import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class FrameBuffer {

	private static final String LIBNAME = "javaFBlib";
    
	private static final int FPS = 60;        

	private final String deviceName;

	private long deviceInfo;        

	private int width, height;

	private int bits;

	private BufferedImage img;

	private int[] imgBuffer;

	private ManualRepaintThread	mrt;

	// -----------------------------------------------------------------------------------------------------------------

	private native long openDevice(String device);

	private native void closeDevice(long di);

	private native int getDeviceWidth(long di);

	private native int getDeviceHeight(long di);

	private native int getDeviceBitsPerPixel(long di);

	private native boolean updateDeviceBuffer(long di, int[] buffer);

	static {
		System.loadLibrary(LIBNAME);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Open the named frame buffer device and starts the automatic update thread between the internal
	 * BufferedImage and the device.
	 *
	 * @param deviceName
	 */
	public FrameBuffer(String deviceName) {
		this(deviceName, true);
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Open the named frame buffer device.
	 *
	 * @param deviceName 
	 * @param autoUpdate if true, starts the automatic update thread between the internal
	 *                   BufferedImage and the device. If false, you have to call repaint();
	 */
	public FrameBuffer(String deviceName, boolean autoUpdate) {

		this.deviceName = deviceName;

		deviceInfo = openDevice(deviceName);

		if (Math.abs(deviceInfo) < 10) {
			throw new IllegalArgumentException("Init. for frame buffer " + deviceName + " failed with error code " + deviceInfo);
		}

		this.width  = getDeviceWidth(deviceInfo);
		this.height = getDeviceHeight(deviceInfo);

		System.err.println("Open with " + deviceName + " (" + deviceInfo + ")");
		System.err.println("  width   " + getDeviceWidth(deviceInfo));
		System.err.println("  height  " + getDeviceHeight(deviceInfo));
		System.err.println("  bpp     " + getDeviceBitsPerPixel(deviceInfo));

		// We always use ARGB image type.
		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		imgBuffer = ((DataBufferInt) img.getRaster().getDataBuffer()).getBankData()[0];

		if (autoUpdate)
			new AutoUpdateThread().start();
		else {
			mrt = new ManualRepaintThread();
			mrt.start();
		}
	}

	public abstract void onRepaint();
	
	/**
	 * Internal helper class for refreshing the frame buffer display.
	 */
	private class AutoUpdateThread extends Thread {

		AutoUpdateThread() {
			setDaemon(true);
			setName("FB " + deviceName + " update");
		}

		@Override
		public void run() {
			final int SLEEP_TIME = 1000 / FPS;

			// System.err.println("Run Update");
			while (deviceInfo != 0) {

				try{
				    onRepaint();
				}catch(Exception ex){
				    
				}
				updateScreen();

				try {
					sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					break;
				}

			}    
		}

	}    

	// -----------------------------------------------------------------------------------------------------------------

	private final ArrayBlockingQueue<Boolean> repaintQueue = new ArrayBlockingQueue<>(1);

	/**
	 * Request an repaint manually. This method can called at high frequencies. An internal repaint tread is used to
	 * avoid exceeding the FPS value.
	 */
	public void repaint() {
		if (mrt == null) throw new IllegalStateException("automatic repaint is active, no need to call this");
		repaintQueue.offer(Boolean.TRUE);
	}

	/**
	 * Internal helper class for refreshing the frame buffer display and/or JPanel.
	 */
	private class ManualRepaintThread extends Thread {

		ManualRepaintThread() {
			setDaemon(true);
			setName("FB " + deviceName + " repaint");
		}

		@Override
		public void run() {
			final int SLEEP_TIME = 1000 / FPS;

			try {
				System.err.println("Run Repaint");
				while (deviceInfo != 0) {

					repaintQueue.take();
					updateScreen();

					sleep(SLEEP_TIME);

				}    
			} catch (InterruptedException e) {
			}

		}    
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Returns the BufferedImage for drawing. Anything your draw here is synchronized to the frame buffer.
	 *
	 * @return BufferedImage of type ARGB.
	 */
	public BufferedImage getScreen() {
		return img;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Close the device.
	 */
	public void close() {
		synchronized (deviceName) {
			closeDevice(deviceInfo);
			deviceInfo = 0;
			img = null;
			imgBuffer = null;
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private long	lastUpdate;
	private	int		updateCount;

	/**
	 * Update the screen if no automatic sync is used (see constructor autoUpdate flag).
	 * This method is normally called by the autoUpdate thread and is not limited about any frame rate.
	 *
	 * @return true if the BufferedImage was changed since the last call.
	 */
	public boolean updateScreen() {


		synchronized (deviceName) {
			if (deviceInfo == 0) return false;

			boolean ret;
			synchronized (updateLock) {

				ret = updateDeviceBuffer(deviceInfo, imgBuffer);

				updateCount++;
				if (lastUpdate == 0) lastUpdate = System.currentTimeMillis();
				long now = System.currentTimeMillis();

				long diff = now - lastUpdate;

				if (diff >= 1000) {
					float fps = (1000f / diff) * updateCount;
//					System.err.println("FPS = "+fps);
					updateCount = 0;
					lastUpdate  = now;
				}

			}
			return ret;
		}    
	}

	private final Object updateLock = new Object();

	public Object getUpdateLockForSync() {
		return updateLock;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}    
