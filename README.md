# Java FB lib #

C lib to refresh framebuffer device with java BufferedImage via JNI.


```
#!java

        final FrameBuffer fb;
        final BufferedImage img;
        Graphics2D g;
...
        fb = new FrameBuffer("/dev/fb0", true){
	    @Override
	    public void onRepaint() {
		//Draw here on g
	    }   
	};
	img = fb.getScreen();
	g = img.createGraphics();

```


From:
https://github.com/ttww/JavaFrameBuffer
https://github.com/esialb/JavaFrameBuffer