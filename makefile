CC = gcc
CFLAGS = -Wall -O3 -I/usr/lib/jvm/java-8-openjdk-armhf/include -I/usr/lib/jvm/java-8-openjdk-armhf/include/linux -shared -fPIC

all:
	$(CC)  -o javaFBlib.so FrameBuffer.c $(CFLAGS)
	cp ./javaFBlib.so /lib
